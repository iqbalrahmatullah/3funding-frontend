/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        "primary": "#35A2C0",
        "secondary": "#CBE9F1"
      },
    },
    container: {
      center: true,
    }
  },
  plugins: [
    require('flowbite/plugin')
  ],
}

