import { createApp } from 'vue'
import './style.css'
import 'animate.css'
import App from './App.vue'
import router from './router'
import NavbarComponent from "./components/Navbar.vue"
import Footer from "./components/Footer.vue"

createApp(App).use(router).component('NavbarComponent', NavbarComponent).component('Footer', Footer).mount('#app')