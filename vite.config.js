import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  devServer: {
    proxy: "https://3funding.ajakjago.com/"
  },
  plugins: [vue()],
  server: {
    cors: false
  }
})
