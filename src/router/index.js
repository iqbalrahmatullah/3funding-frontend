import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import AllCampaign from '../views/AllCampaign.vue'
import Contact from '../views/Contact.vue'
import TentangKami from '../views/TentangKami.vue'
import PerkembanganCampaign from "../views/PerkembanganCampaign.vue"
import DetailPerkembangan from "../views/DetailPerkembangan.vue"
import Checkout from "../views/Checkout.vue"
import DetailCampaign from '../views/DetailCampaign.vue'
import FormPendaftaran from '../views/FormPendaftaran.vue'
import UserDashboard from '../views/UserDashboard/Index.vue'
import dashboardDetailCampaign from "../views/UserDashboard/DetailCampaign.vue"
import HomeDashboard from "../views/UserDashboard/Home.vue"
import CreateCampaign from "../views/UserDashboard/CreateCampaign.vue"
import FormPencairan from "../views/UserDashboard/FormPencairan.vue"
import FormPerkembangan from "../views/UserDashboard/FormPerkembangan.vue"
import TransactionDashboard from "../views/UserDashboard/Transaction.vue"
import Login from "../views/Login.vue"
import CampaignDashboard from '../views/UserDashboard/Campaign.vue'
import NotFound from "../views/NotFound.vue"
import Swal from 'sweetalert2'

const routes = [
    {
        path: '',
        name: 'Home',
        component: Home,
        meta: {
            title: 'Homepage'
        }
    },
    {
        path: '/campaigns',
        name: 'Campaigns',
        component: AllCampaign,
        meta: {
            title: 'Semua Campaign'
        }
    },
    {
        path: '/perkembangan-campaigns',
        name: 'PerkembanganCampaigns',
        component: PerkembanganCampaign,
        meta: {
            title: 'Perkembangan Campaign'
        }
    },
    {
        path: '/perkembangan-campaigns/:id',
        name: 'DetailPerkembanganCampaign',
        component: DetailPerkembangan,
        meta: {
            title: 'Perkembangan Campaign'
        }
    },
    {
        path: '/campaign/:id',
        name: 'DetailCampaign',
        component: DetailCampaign,
        meta: {
            title: 'Detail Campaign'
        }
    },
    {
        path: '/contact',
        name: 'Contact',
        component: Contact,
        meta: {
            title: 'Kontak Kami'
        }
    },
    {
        path: '/checkout/:id',
        name: 'Checkout',
        component: Checkout,
        meta: {
            title: 'Checkout Page'
        },
        beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('token_transaction')) {
                next(from.path)
            } else {
                next()
            }
        }
    },
    {
        path: '/about',
        name: 'About',
        component: TentangKami,
        meta: {
            title: 'Tentang Kami'
        }
    },
    {
        path: '/register',
        name: 'FormPendaftaran',
        component: FormPendaftaran,
        meta: {
            title: 'Pendaftaran Campaign',
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            title: 'Login',
        },
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('token')) {
                next('/dashboard')
            } else {
                next()
            }
        }
    },
    {
        path: '/dashboard',
        name: 'UserDashboard',
        redirect: '/dashboard/home',
        component: UserDashboard,
        meta: {
            title: 'Dashboard',
        },
        beforeEnter: (to, from, next) => {
            if (localStorage.getItem('token')) {
                next()
            } else {
                Swal.fire({
                    title: "Error!",
                    text: "Harap login terlebih dahulu!",
                    icon: "error",
                    confirmButtonText: "OK",
                })
                next('/login')
            }
        },
        children: [
            {
                path: "home",
                component: HomeDashboard
            },
            {
                path: "campaign",
                component: CampaignDashboard,
                meta: {
                    title: 'Campaign',
                }
            },
            {
                path: "create-campaign",
                component: CreateCampaign,
                meta: {
                    title: 'Create Campaign',
                }
            },
            {
                path: "transaction",
                component: TransactionDashboard,
                meta: {
                    title: 'All Transaction',
                }
            },
            {
                path: "detailcampaign/:id",
                component: dashboardDetailCampaign,
                meta: {
                    title: 'Detail Campaign',
                }
            },
            {
                path: "pencairan/:id",
                component: FormPencairan,
                meta: {
                    title: 'Pencairan Campaign',
                }
            },
            {
                path: "perkembangan/:id",
                component: FormPerkembangan,
                meta: {
                    title: 'Perkembangan Campaign',
                }
            },
        ]
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: NotFound,
        meta: {
            title: 'Not Found',
        },
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            return { top: 0 };
        }
    }
})

router.beforeEach((to, from) => {
    document.title = to.meta.title ? "3Funding | " + to.meta.title : "3Funding"
})


export default router